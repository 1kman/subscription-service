package subscriptionservice.model;

import java.util.HashSet;

import subscriptionservice.model.Message.MessageType;

public class Subscriber {
	
	public String username;
	public HashSet<MessageType> subscriptions = new HashSet<MessageType> ();
	
	/**
	 * This is the index of the last message the user read.
	 */
	public long lastMessageIndex = 0;
	

	public Subscriber() {
		super();
	}

	public Subscriber(String username, HashSet<MessageType> subscriptions) {
		super();
		this.username = username;
		this.subscriptions = subscriptions;
	}

	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public HashSet<MessageType> getSubscriptions() {
		return subscriptions;
	}
	public void setSubscriptions(HashSet<MessageType> subscriptions) {
		this.subscriptions = subscriptions;
	}
	public long getLastMessageIndex() {
		return lastMessageIndex;
	}
	public void setLastMessageIndex(long lastMessageIndex) {
		this.lastMessageIndex = lastMessageIndex;
	}

	
}
