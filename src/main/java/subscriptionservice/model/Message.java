package subscriptionservice.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Message implements Serializable {
	
	public enum MessageType { FAMILY, WORK, FUN }
	
	@JsonProperty("id")
    private  long id;
	@JsonProperty("messgeType")
    private MessageType messgeType;
	@JsonProperty("content")
    private  String content;
	@JsonProperty("createDate")
    private Date createDate;
	@JsonProperty("username")
    private String username;
    
	public Message() {
		super();
	}
	
	public Message(long id, MessageType messgeType, String content, Date createDate, String username) {
		super();
		this.id = id;
		this.messgeType = messgeType;
		this.content = content;
		this.createDate = createDate;
		this.username = username;
	}
	
//	@JsonCreator
//	public Message(@JsonProperty("messgeType") String messageType,
//	                  @JsonProperty("content") String content,
//	                  @JsonProperty("username") String username) {
//		this.messgeType = MessageType.valueOf(messageType);
//		this.content = content;
//		this.username = username;
//	}


//	public Message(MessageType messgeType, String content, String username) {
//		super();
//		this.messgeType = messgeType;
//		this.content = content;
//		this.username = username;
//	}
	
//	@Override
//	public Message(String json) {
//		Message message = ObjectMapper().readValue(json, Message.class);
//	}
//
//	@JsonCreator
//	public static Message Create(String jsonString)
//	{
//
//		Message message = null;
//	    try {
//	    	message = mapper.readValue(jsonString, Message.class);
//	    } catch (JsonParseException|JsonMappingException|IOException e) {
//	        // handle
//	    }
//
//	    return pc;
//	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public MessageType getMessgeType() {
		return messgeType;
	}
	public void setMessgeType(MessageType messgeType) {
		this.messgeType = messgeType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
    
    

}
