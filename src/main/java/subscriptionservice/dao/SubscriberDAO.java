package subscriptionservice.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import subscriptionservice.model.Message;
import subscriptionservice.model.Subscriber;
import subscriptionservice.model.Message.MessageType;

@Component
public class SubscriberDAO {
	
	private static Map<String, Subscriber> subscribers = new HashMap<String, Subscriber>();
	
	static {
		subscribers.put("ALL", new Subscriber("ALL", new HashSet<MessageType> (Arrays.asList(Message.MessageType.FAMILY,Message.MessageType.WORK,Message.MessageType.FUN)))); 
		subscribers.put("WORK", new Subscriber("WORK", new HashSet<MessageType> (Arrays.asList(Message.MessageType.WORK)))); 
		subscribers.put("WORK_FUN", new Subscriber("WORK_FUN", new HashSet<MessageType> (Arrays.asList(Message.MessageType.WORK,Message.MessageType.FUN)))); 
	}
	
	private SubscriberDAO() {
		
	}
		
	public void addSubscriber(Subscriber subscriber) {
		subscribers.put(subscriber.username, subscriber);
	}

	public Map<String, Subscriber> getSubscribers() {
		return subscribers;
	}
	
	public Subscriber getSubscriber(String username) {
		return subscribers.get(username);
	}
	
	public boolean subscriberExists(String username) {
		return subscribers.get(username) != null;
	}

	
}
