package subscriptionservice.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Component;

import subscriptionservice.model.Message;

@Component
public class MessageDAO {
	
	private static List<Message> messages = new ArrayList<Message>();
	
	static {
		messages.add(new Message(1, Message.MessageType.FAMILY, "Home is the place where, when you have to go there, they have to take you in.", Calendar.getInstance().getTime(), "ADMIN"));
		messages.add(new Message(2, Message.MessageType.WORK, "There are no secrets to success. It is the result of preparation, hard work, and learning from failure.", Calendar.getInstance().getTime(), "ADMIN"));
		messages.add(new Message(3, Message.MessageType.FUN, "In our leisure we reveal what kind of people we are.", Calendar.getInstance().getTime(), "ADMIN"));
	}
	
	private MessageDAO() {
		
	}
		
	public void addMessage(Message message) {
		messages.add(message);
	}

	public List<Message> getMessages() {
		return messages;
	}

	
}
