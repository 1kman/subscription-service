package subscriptionservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import subscriptionservice.dao.MessageDAO;
import subscriptionservice.dao.SubscriberDAO;
import subscriptionservice.model.Message;
import subscriptionservice.model.Subscriber;

@RestController
public class SubscriptionController {
	
	@Autowired
	private MessageDAO messageDao;

	@Autowired
	private SubscriberDAO subscriberDao;


    @RequestMapping("/messages")
    public ResponseEntity getMessages(@RequestParam(value="username") String username) {
    	//return messageDao.getMessages();    
    	if(!subscriberDao.subscriberExists(username)) {
    		return new ResponseEntity("unknown username: " + username,HttpStatus.BAD_REQUEST);
    	}
    	
    	Subscriber profile = subscriberDao.getSubscriber(username);
    	List<Message> result = new ArrayList<Message>();
    	// Move this logic to DAO instead of expensive looping.
    	for(Message message : messageDao.getMessages()) {
    		if(profile.getSubscriptions().contains(message.getMessgeType())) {
    			result.add(message);
    		}
    	}
    	return new ResponseEntity(result, HttpStatus.OK);
    
    }
    
    @PostMapping("/messages")
    public String putMessage(@RequestBody Message message) {
    	message.setCreateDate(Calendar.getInstance().getTime());
    	// TODO add validation that user exists.
    	// TODO set id on message.
    	
    	messageDao.addMessage(message);
        return "";//"redirect:/messages";
    }
    
    @PutMapping("/subscriber")
    public ResponseEntity putSubscriber(@RequestParam(value="username") String username, @RequestBody Subscriber subscriber) {
		subscriberDao.addSubscriber(subscriber);
		return new ResponseEntity(HttpStatus.OK);
    }


//    @PutMapping("/message")
//    public String add(@PathVariable String username, @RequestBody Message message) {
//    	message.setCreateDate(Calendar.getInstance().getTime());
//    	// TODO add validation that user exists.
//    	message.setUsername(username);
//    	// TODO set id on message.
//    	
//        MessageStore.getInstance().addMessage(message);
//        return "";//"redirect:/appointments";
//    }

}
